﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class imputShip : MonoBehaviour {

    private Vector2 axis;
    public PlayerBehaviour player;
    public Weapons weapons;

	void Start () {
		
	}
	
	void Update () {
        axis.x = Input.GetAxis("Horizontal");
        axis.y = Input.GetAxis("Vertical");
        player.setAxis(axis);

        if(Input.GetButton("Jump"))
        {
            weapons.ShotWeapon();
        }

        if(Input.GetKeyDown(KeyCode.Z))
        {
            weapons.PreviousWeapon();
        }
    }
}