﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoCorto : Shoot
{
    Cartridge cartridge;

    public float timeToExplode = 1;
    private float currentTime = 0f;

    new void Update()
    {
        if (shooting)
        {
            transform.Translate(0, speed * Time.deltaTime, 0);

            currentTime += Time.deltaTime;
            if (currentTime > timeToExplode)
            {
                Reset();
                currentTime = 0f;
            }
        }
    }
}