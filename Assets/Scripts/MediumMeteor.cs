﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediumMeteor : TinyMeteor
{

    protected override void Explode()
    {
        MeteorManager.instance.LaunchMeteor(2, transform.position, new Vector2(Random.Range(-5, 4), Random.Range(-2, 2)), 20);
        MeteorManager.instance.LaunchMeteor(2, transform.position, new Vector2(Random.Range(-5, 4), Random.Range(-2, 2)), 20);
        base.Explode();
    }
}
