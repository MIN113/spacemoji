﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escopeta : Shoot
{
    Cartridge cartridge;

    new void Start()
    {
        base.Start();
        cartridge = GameObject.Find("Weapon").GetComponent<Weapons>().cartridges[0];
    }


    public override void Shot(Vector3 position, float direction)
    {
            Vector3 tmp = position;
        cartridge.GetBullet().Shot(tmp, Random.Range(-30, 30));
        cartridge.GetBullet().Shot(tmp, Random.Range(-30, 30));
        cartridge.GetBullet().Shot(tmp, Random.Range(-30, 30));
        cartridge.GetBullet().Shot(tmp, Random.Range(-30, 30));
        cartridge.GetBullet().Shot(tmp, Random.Range(-30, 30));
        cartridge.GetBullet().Shot(tmp, Random.Range(-30, 30));
        cartridge.GetBullet().Shot(tmp, Random.Range(-30, 30));
        Reset();       
    }
}
    
    