﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGun : MonoBehaviour {

	public GameObject EnemyBulletG;

	void Start () {
		Invoke ("FireEnemyBullet", 1F);
	}

	void Update () {
		
	}

	void FireEnemyBullet(){
		GameObject playerShip = GameObject.Find("Ship");

		if (playerShip != null) {
			GameObject bullet = (GameObject)Instantiate (EnemyBulletG);
			bullet.transform.position = transform.position;
			Vector2 direction = playerShip.transform.position - bullet.transform.position;

			bullet.GetComponent<EnemyBullet> ().SetDirection (direction);
		} 
	}
}
