﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnSpawner : MonoBehaviour {


	public GameObject enemy;
	public GameObject enemyG;
	public float spawnTime = 3f;
	public Transform spawnPoint; 

	void Start ()
	{
		InvokeRepeating ("Spawn", spawnTime, spawnTime);
			}


	void Spawn ()
	{		
		Instantiate (enemy, spawnPoint.position, spawnPoint.rotation);
		Instantiate (enemyG, spawnPoint.position, spawnPoint.rotation);
	}
}