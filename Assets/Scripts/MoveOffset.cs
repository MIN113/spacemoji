﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveOffset : MonoBehaviour {

    private Material mat;
    public float speed;
    private Vector2 vec;

	void Awake () {
         mat = GetComponent<MeshRenderer>().material;
        }
	
	void Update () {
        vec.y += speed * Time.deltaTime;

        if (vec.y > 100)
        {
            vec.y -= 100;
        }

        mat.SetTextureOffset("_MainTex", vec);
	}
}
