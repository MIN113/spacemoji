﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System;

public class ButtonBehaviour : MonoBehaviour, ISelectHandler, IPointerEnterHandler, IPointerExitHandler
{
    private int counter = 0;
    // When highlighted with mouse.
    public Text pussy;
    public void OnPointerEnter(PointerEventData eventData)
    {
        pussy.text = "Don't be a pussy";
        // Do something.
        Debug.Log("<color=red>Event:</color> Completed mouse highlight.");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        pussy.text = "EXIT";
    }

    // When selected.
    public void OnSelect(BaseEventData eventData)
    {
        if (counter < 5)
        {
            transform.Translate(new Vector3(30, 30, 0));
            // Do something.
            Debug.Log("<color=red>Event:</color> Completed selection.");
            counter++;

        }

        else if (counter >= 5)
        {
            SceneManager.LoadScene("MainMenu");
        }
        
    }
}