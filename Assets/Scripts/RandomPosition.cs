﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPosition : MonoBehaviour {

	public Vector2 axis;

	private Vector3 positionActual;

	void Update() {
		transform.Translate (axis * Time.deltaTime);
		positionActual = transform.position;
	

		if (positionActual.x > 7) {
			axis.x -= Random.Range (2, 7);
		}
		if (positionActual.x < -7) {
			axis.x -= Random.Range (-2, -7);
			}

		transform.position = positionActual;
	}
}