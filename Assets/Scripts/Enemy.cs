﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
	private Transform player;
	public float rotSpeed;
	public float speed;

	public AudioSource audiop;

	public GameObject graphics;
	public ParticleSystem explosion;

	public static SpaceManager instance;
	private BoxCollider2D myCollider;

	public static SpaceManager getInstance(){
	return instance;
	}

	void Awake()
	{
		myCollider = GetComponent <BoxCollider2D>();
		player = GameObject.Find("Ship").GetComponent<Transform>();
	}

	protected void Update()
	{
		Vector3 pos = transform.position;
		Vector3 velocity = new Vector3 (0, speed * Time.deltaTime);
		pos += transform.rotation * velocity;
		transform.position = pos;


		Vector3 dir = player.position - transform.position;
		dir.Normalize ();

		float zAngle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg - 90;

		Quaternion desiredRot = Quaternion.Euler (0, 0, zAngle);
		transform.rotation = Quaternion.RotateTowards (transform.rotation, desiredRot, rotSpeed * Time.deltaTime);
	}

	private void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Bullet" || other.tag == "Player") {
			Explode ();
		}
		else if (other.tag == "Finish") {
			Reset ();
		}
	}
	private void Explode(){
		myCollider.enabled = false;
		SpaceManager.instance.AddHighScore(100);
		speed = 0;
		graphics.SetActive(false);
		explosion.Play();
		audiop.Play();
		Destroy (explosion, 1);
		Destroy (gameObject, 3);
	}

	protected void Reset()
	{
		myCollider.enabled = true;
		speed = 5;
		graphics.SetActive(true);
		Destroy (gameObject, 3);
	}
}
