﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragmentBullet : Shoot
{

    //public int maxAmmo = 2;
    Cartridge cartridge;
    public float timeToExplode = 1;
    private float currentTime = 0f;

    new void Start()
    {
        base.Start();
        cartridge = GameObject.Find("Weapon").GetComponent<Weapons>().cartridges[2];
    }

    protected new virtual void Update()
    {
        base.Update();
        if (shooting)
        {
            currentTime += Time.deltaTime;
            if (currentTime > timeToExplode)
            {
                ShotMiniBullets();
                
            }
        }
        else
        {
            currentTime = 0f;
            
        }
    }

    void ShotMiniBullets()
    {
        Debug.Log("Shot mini bullets");

        cartridge.GetBullet().Shot(transform.position, Random.Range(0, 359));
        cartridge.GetBullet().Shot(transform.position, Random.Range(0, 359));
        cartridge.GetBullet().Shot(transform.position, Random.Range(0, 359));
        cartridge.GetBullet().Shot(transform.position, Random.Range(0, 359));
        cartridge.GetBullet().Shot(transform.position, Random.Range(0, 359));
        cartridge.GetBullet().Shot(transform.position, Random.Range(0, 359));
        cartridge.GetBullet().Shot(transform.position, Random.Range(0, 359));
        cartridge.GetBullet().Shot(transform.position, Random.Range(0, 359));
        cartridge.GetBullet().Shot(transform.position, Random.Range(0, 359));
        cartridge.GetBullet().Shot(transform.position, Random.Range(0, 359));
        cartridge.GetBullet().Shot(transform.position, Random.Range(0, 359));
        cartridge.GetBullet().Shot(transform.position, Random.Range(0, 359));

        Reset();
    }
}